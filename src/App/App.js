import React, {useEffect, useState } from 'react';
import './App.css';
import {DndProvider} from "react-dnd"
import {HTML5Backend} from 'react-dnd-html5-backend';
import Footer from '../Components/Footer/Footer';
import Board from '../Components/Board/Board';
import LandingPage from '../Components/LandingPage/LandingPage';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBLink, MDBNavLink, MDBNavbarToggler, MDBCollapse } from "mdbreact";
import ModalLogin from '../Components/ModalLogin/ModalLogin';
import ModalRegister from '../Components/ModalRegister/ModalRegister';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Boards from '../Components/Boards/Boards';
import axios from 'axios';

export default function App() {
    const [registerModalState, setRegisterModalState] = useState();
    const [myBoards, setMyBoards] = useState([]);
    const [fillMyBoards, setFillMyBoard] = useState(0);
    const [loggedIn, setLoggedIn] = useState(false);
    const [username, setUsername] = useState("");
    const state = { isOpen: false };
    
    function toggleCollapse(){
      this.setState({ isOpen: !this.state.isOpen });
    }

    useEffect(()=> {
        checkIfLoggedIn();
    }, [checkIfLoggedIn])


    function checkIfLoggedIn(){
        axios
        .get(process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/user/me': process.env.REACT_APP_PRO_MODE + '/user/me',{
            headers: {
                'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
             }
            }).then((result) => {
                setLoggedIn(true);
                setUsername(result.data.username);
                localStorage.setItem("userid", result.data.id);
                
                // Get all the board ID that uses to this user id
                result.data.boards.map(i => {
                    if(myBoards.length !== result.data.boards.length){
                        myBoards.push(i.id);
                    }
                });

                localStorage.setItem("boardids", myBoards);

            }).catch(error => {
                setLoggedIn(false);
                console.log(error)
            });
    }

    function logout(){
        localStorage.clear();
        alert("Je bent uitgelogd")
        checkIfLoggedIn();
    }

    return (
        <DndProvider backend={HTML5Backend}>
            <Router>
                <MDBNavbar
                    color="main-color"
                    expand="md"
                    style={{}}>
                    <MDBNavbarBrand>
                      <MDBNavLink to="/"><strong>TiTask</strong></MDBNavLink>
                    </MDBNavbarBrand>
                    <MDBNavbarToggler onClick={toggleCollapse}/>
                    <MDBCollapse id="navbarCollapse3" isOpen={state.isOpen} navbar>
                        <MDBNavbarNav left>
                        </MDBNavbarNav>
                        <MDBNavbarNav right>
                            {loggedIn ?  (
                                <>
                                <div className={"nav-link Ripple-parent"}>Hello {username}</div>
                                <MDBLink onClick={() => logout()} to="/">Logout</MDBLink>
                                </>
                            ) : (
                                <>
                                <ModalLogin checkIfLoggedIn={checkIfLoggedIn} />
                                <ModalRegister checkIfLoggedIn={checkIfLoggedIn}/>
                            </>
                            )}
                        </MDBNavbarNav>
                    </MDBCollapse>
                </MDBNavbar>

                <Switch>
                    <Route exact path="/">
                        <LandingPage/>
                    </Route>
                    <Route exact path="/boards">
                        <Boards />
                    </Route>
                    <Route exact path="/board/:id" component={Board}>
                        <Board />
                    </Route>
                </Switch>
            </Router>
            
            <Footer/>
        </DndProvider>
    );
}

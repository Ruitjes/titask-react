import React, { Fragment, useState, useRef } from "react";
import { useDrag, useDrop, } from "react-dnd";
import EditTaskModal from "./EditTaskModal";
import ITEM_TYPE from "./data/types";


export default function Item({ item, index, moveItem, status, refetch, reload, client, id }) {
    const ref = useRef(null);
    const [, drop] = useDrop({
        accept: ITEM_TYPE,
        hover(item, monitor) {
            if (!ref.current) {
                return
            }
            const dragIndex = item.index;
            const hoverIndex = index;

            if (dragIndex === hoverIndex) {
                return
            }

            const hoveredRect = ref.current.getBoundingClientRect();
            const hoverMiddleY = (hoveredRect.bottom - hoveredRect.top) / 2;
            const mousePosition = monitor.getClientOffset();
            const hoverClientY = mousePosition.y - hoveredRect.top;

            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return;
            }

            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return;
            }
            moveItem(dragIndex, hoverIndex);
            item.index = hoverIndex;
        },
    });

    const [{ isDragging }, drag] = useDrag({
        item: { type: ITEM_TYPE, ...item, index },
        collect: monitor => ({
            isDragging: monitor.isDragging()
        })
    });

    drag(drop(ref));
    
    const [state, setState] = useState(false)

    function toggle(){
        setState(!state);
    }
    return (
        <Fragment>
            <div
                data-testid={item.title.toLowerCase().split(' ').join('-')+'-task-move'}
                ref={ref}
                style={{ opacity: isDragging ? 0 : 1 }}
                className={"item light-color"}
                onClick={() => toggle()}>

                <div className={"color-bar"} style={{ backgroundColor: status.color }}></div>
                <div className={"item-header"}>
                    <p style={{float: "left"}} className={"item-title"}>{item.content}</p>                    
                </div>
                <div style={{clear: "both"}}></div>

                <p className={"item-status"}>{item.icon}</p>
            </div>

            <EditTaskModal id={id} client={client} refetch={refetch} reload={reload} item={item} state={state} toggle={toggle} />

        </Fragment>
    );
}


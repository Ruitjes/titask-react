import React, { useRef, useState } from "react";
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBBox, MDBRow, MDBCol, MDBInput } from 'mdbreact';
import  {useForm}  from "react-hook-form";
import axios from 'axios';


export default function CreateColModal(props){
    const { register, handleSubmit } = useForm();
    const ref = useRef(null);
    const [createColModal, SetCreateColModal] = useState();

    function CreateColModalToggle() {
        SetCreateColModal(!createColModal);
    }
    const onSubmit = (data, e) => {
        e.target.reset();

        const task = {
            title: data.title,
        };
            axios({
                method: 'post',
                url: process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/board/'+props.boardId+'/col': process.env.REACT_APP_PRO_MODE + '/board/'+props.boardId+'/col',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': "Bearer " + localStorage.getItem("jwttoken"),

                },
                data: {
                    icon: "x",
                    status: task.title,
                    color: "#000000",
                }
            }).then(response => {
                CreateColModalToggle();
                props.client.publish({destination: '/app/socket/'+props.id, body: props.id.toString()});
            }).catch(error => {
                console.log(error)
            });
      };

  return (
    <MDBContainer style={{padding: "0px"}}>
    <MDBBox display="flex" justifyContent="end">
        <MDBBtn onClick={CreateColModalToggle} color={"#5d5dff"} className={"light-color"}>Add Column</MDBBtn>
    </MDBBox>
    <MDBModal isOpen={createColModal} toggle={CreateColModalToggle}>
    <MDBModalHeader className={"main-light-color"} toggle={CreateColModalToggle}>Add Column To Board</MDBModalHeader>
    <MDBModalBody className={"main-light-color"}>
        <MDBRow >
            <MDBCol md="6">
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="grey-text">
                <MDBInput label="Column Name" aria-label={"Column Name"} inputRef={register} name="title" required minlength="3" maxlength="15"  />
                <MDBBtn color={"#5d5dff"} name="button" className={"light-color text-color"} innerRef={register} type="submit">Send</MDBBtn>
                </div>
            </form>
            </MDBCol>
        </MDBRow>
    </MDBModalBody>
    <MDBModalFooter className={"main-light-color"}>
        <MDBBtn color="red" onClick={CreateColModalToggle}>Close</MDBBtn>
    </MDBModalFooter>
    </MDBModal>
    </MDBContainer>
    );
}


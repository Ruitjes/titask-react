import React from "react";
import { useDrop } from "react-dnd";
import ITEM_TYPE from "./data/types"

export default function DropWrapper({ onDrop, children, status }){
    const [{ isOver }, drop] = useDrop({
        accept: ITEM_TYPE,
        canDrop: (item, monitor) => {
            // ------- Only drop on the next col -----------
            // const itemIndex = statuses.findIndex(si => si.status === item.status);
            // const statusIndex = statuses.findIndex(si => si.status === status);
            // return [itemIndex + 1, itemIndex - 1, itemIndex].includes(statusIndex);
            return true
        },
        drop: (item, monitor) => {
            onDrop(item, monitor, status);
        },
        collect: monitor => ({
            isOver: monitor.isOver()
        })
    });

    return (
        <div ref={drop} className={"drop-wrapper "+ status.toLowerCase()+"-drop"}>
            {React.cloneElement(children, { isOver })}
        </div>
    )
};


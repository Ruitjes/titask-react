// test/LoginForm.test.js
import '@testing-library/jest-dom'
import React from 'react'
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import Board from '../Board'
import { createMemoryHistory } from 'history'
import { Router } from 'react-router-dom'
import 'fast-text-encoding';

import 'mutationobserver-shim';
global.MutationObserver = window.MutationObserver;

const boardId = 1;
const url = process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + "/board/" + boardId: process.env.REACT_APP_PRO_MODE + "/board/" + boardId

const fakeResponse = {"id":72,"title":"test","description":"test","cols":[{"id":109,"status":"a","icon":"x","color":"#000000"}],"tasks":[{"id":50,"icon":"x","status":"a","taskOrder":null,"title":"a","content":"yeet"}]}

const server = setupServer(
  rest.get(url, (req, res, ctx) => {
    // Respond with a mocked user jwttoken that gets persisted
    // in the `sessionStorage` by the `Login` component.
    return res(ctx.json())
  }),
)

// Enable API mocking before tests.
beforeAll(() => server.listen())

// Reset any runtime request handlers we may add during the tests.
afterEach(() => server.resetHandlers())

// Disable API mocking after the tests are done.
afterAll(() => server.close())

test('allows the user to log in', async () => {
  const history = createMemoryHistory()
  history.push('/board/'+boardId)
  render(
    <Router history={history}>
      <Board />
    </Router>
  )  // userEvent.click(screen.getByText("Login"));

  // userEvent.type(screen.getByLabelText("Your name"), username)
  // userEvent.type(screen.getByLabelText("Your password"), password)
  
  // userEvent.click(screen.getByText("Login Gebruiker"));
 
  // expect('abc').toEqual(fakeResponse.tasks.content)
})

import React, {useState, useEffect, useCallback, useRef} from "react";
import Item from "./Item";
import DropWrapper from "./DropWrapper";
import Col from "./Col";
import {MDBContainer, MDBRow, MDBCol, MDBBox} from "mdbreact";
import CreateTaskModal from './CreateTaskModal';
import CreateColModal from './CreateColModal';
import axios from 'axios';
import {useParams} from "react-router-dom";
import { Client } from '@stomp/stompjs';

export default function Board(props) {
    const [items, setItems] = useState([]);
    const [boardName, setBoardName] = useState("");
    const [boardId, setBoardId] = useState(0);
    const [order, setTaskOrder] = useState(0);
    const [firstStatus, setFirstStatus] = useState("");
    const [client, setClient] = useState(() => new Client())
    const [statuses, setStatuses] = useState([]);
    const {id} = useParams();

    useEffect(() => {
        //Is this my board
        axios
        .get(process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE +"/user/" + localStorage.getItem("userid") +"/myboard/"+id : process.env.REACT_APP_PRO_MODE +"/user/" + localStorage.getItem("userid") +"/myboard/"+id,{
            headers: {
                'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
            }
        }).then((result) => {
            if(result.data){
                refetch();
            }else{
                alert("Geen toegang, is dit wel jouw bord?");
                window.location.href = "/";
            }
        }).catch(error => {
            alert("Er is iets fout gegaan")
            window.location = '/';
        });
        
        // The compat mode syntax is totally different, converting to v5 syntax
        // Client is imported from '@stomp/stompjs'
       client.configure({
          brokerURL: process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_SOCKET_MODE + '/stomp' : process.env.REACT_APP_PRO_SOCKET_MODE +'/stomp',
        // brokerURL: 'ws://localhost:8081/stomp',
          onConnect: () => {
            console.log('onConnect');
           client.subscribe('/board/socket/' + id, callback => {
                refetch();
            });

           client.publish({destination: '/app/socket/'+id, body: id.toString()});
          },
          // Helps during debugging, remove in production
          debug: (str) => {
            console.log(new Date(), str);
          }
        });
    
       client.activate();
    }, []);

    function refetch(){
        axios
            .get(process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + "/board/" + id: process.env.REACT_APP_PRO_MODE + "/board/" + id,{
                headers: {
                    'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
                }
            })
            .then((result) => {
                console.log(JSON.stringify(result.data));
                if(result.data.cols[0] != null) {
                    setFirstStatus(result.data.cols[0].status);
                }
                setBoardName(result.data.title)
                setBoardId(result.data.id)
                setItems(result.data.tasks)
                setStatuses(result.data.cols)
            }).catch(error => { 
                console.log(error);
            });
    }

    function deleteCol(i, status){
        if(window.confirm("Weet je zeker dat je column "+ status +" wilt verwijderen?")){
            axios({
                method: 'DELETE',
                url: process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/col/'+i+'/delete': process.env.REACT_APP_PRO_MODE + '/col/'+i+'/delete',
                headers: {
                    'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
                }
            }).then(response => {
                client.publish({destination: '/app/socket/'+id, body: id.toString()});
            }).catch(error => {
                alert(error);
                console.log(error)
            }); 
        }   
    }

    function onDrop(item, monitor, status) {
        const mapping = statuses.find(si => si.status === status);
        setItems(prevState => {
            const newItems = prevState
                .filter(i => i.id !== item.id)
                .concat({
                    ...item,
                    status,
                    icon: mapping.icon
                });

            // Only refetched when status is being changed
            if(item.status !== status){
                console.log(status);
                axios({
                    method: 'PUT',
                    url: process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/task/' + item.id + '/update': process.env.REACT_APP_PRO_MODE + '/task/' + item.id + '/update',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
                    },
                    data: {
                            icon: item.icon,
                            status: status,
                            taskOrder: order,
                            title: item.title,
                            content: item.content      
                    }
                }).catch(error => {
                    console.log(error)
                }).then(response => {
                    // this is now called!
                    client.publish({destination: '/app/socket/'+id, body: id.toString()});
                });
            }

            return [...newItems];
        });
    }

    function moveItem(dragIndex, hoverIndex) {
        const item = items[dragIndex];
        setItems(prevState => {
            const newItems = prevState.filter((i, idx) => idx !== dragIndex);
            newItems.splice(hoverIndex, 0, item);
            setTaskOrder(hoverIndex);

            return [...newItems];
        });
    }

    const style = {
        row: {
            backgroundColor: "#51d2c5"
        },
        col: {
            border: "1px solid black",
            maxWidth: "100%" 
        }
    };

    function goBack(){
        window.location.href = "/boards"
    } 
    
    return (
        <MDBContainer style={{
            padding: "125px 0px 125px 0px"
        }}>
            <MDBBox display="flex" flex="row" justifyContent="center">
                <h1>Welcome to board: {boardName}</h1>
            </MDBBox>
            <MDBBox display="flex" flex="row" justifyContent="center">
                <h6 style={{color: "blue", cursor: "pointer"}} onClick={() => goBack()}>{"< Back to the boards"}</h6>
            </MDBBox>

            <CreateTaskModal
                firstStatus={firstStatus}
                boardId={boardId}
                refetch={refetch}
                client={client}
                statuses={statuses}
                id={id}/>
            
            <CreateColModal
                boardId={boardId}
                refetch={refetch}
                client={client}
                id={id}/>   

            <MDBRow>
                {statuses.map(s => {
                    return (
                        <MDBCol key={s.status} className={"main-light-color"} style={style.col} md="3">
                            <h2 className={"col-header text-center"}>{s
                                    .status.toUpperCase() } 

                                <a data-testid={s.status.toLowerCase()+"-delete"} onClick={() => {deleteCol(s.id, s.status)}} style={{float: "right"}}>x</a>  
                            </h2>
                            <DropWrapper onDrop={onDrop} status={s.status}>
                                <Col>
                                    {items
                                        .filter(i => i.status === s.status)
                                        .map((i, idx) => <Item
                                            refetch={refetch}
                                            key={i.id}
                                            item={i}
                                            index={idx}
                                            moveItem={moveItem}
                                            status={s}
                                            client={client}
                                            id={id}
                                            />)}
                                </Col>
                            </DropWrapper>
                        </MDBCol>
                    )
                })}
            </MDBRow>
        </MDBContainer>
    );
}

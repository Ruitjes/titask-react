import React, { useRef, useState } from "react";
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBBox, MDBRow, MDBCol, MDBInput, MDBAlert } from 'mdbreact';
import  {useForm}  from "react-hook-form";
import axios from 'axios';


export default function CreateTaskModal(props){
    const { register, handleSubmit } = useForm();
    const ref = useRef(null);
    const [taskError, setTaskError] = useState("");
    const [createTaskState, setCreateTaskState] = useState();

    function CreateTaskStateToggle() {
        setCreateTaskState(!createTaskState);
        setTaskError("")
    }

    const onSubmit = (data, e) => {
        if(props.statuses.length != 0) {
            e.target.reset();

            const task = {
                icon: "x",
                status: props.firstStatus,
                title: data.title,
                content: data.content,
            };
                axios({
                    method: 'post',
                    url: process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/board/'+props.boardId+'/task': process.env.REACT_APP_PRO_MODE + '/board/'+props.boardId+'/task',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
    
                    },
                    data: {
                        icon: task.icon,
                        status: task.status,
                        taskOrder: null,
                        title: task.title,
                        content: task.content,
                    }
                }).then(response => {
                    props.client.publish({destination: '/app/socket/'+props.id, body: props.id.toString()});
                    setTaskError("");
                    CreateTaskStateToggle();
                }).catch(error => {
                    alert(error);
                    console.log(error)
                });
        }else{
            setTaskError("Maak eerst een column aan voordat je een task aanmaakt");
        }

      };

  return (
    <MDBContainer style={{padding: "0px"}}>
    <MDBBox display="flex" justifyContent="end">
        <MDBBtn onClick={CreateTaskStateToggle} color={"#5d5dff"} className={"light-color"}>Add Task</MDBBtn>
    </MDBBox>
    <MDBModal isOpen={createTaskState} toggle={CreateTaskStateToggle}>
    <MDBModalHeader className={"main-light-color"} toggle={CreateTaskStateToggle}>Add Task To Board</MDBModalHeader>
    <MDBModalBody className={"main-light-color"}>
        <MDBRow >
            <MDBCol md="6">
            { taskError ? (<> <MDBAlert> {taskError} </MDBAlert> </>): (<></>) }

            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="grey-text">
                <MDBInput label="Task Name" aria-label={"Task Name"} inputRef={register} name="title" required minlength="3" maxlength="25"  />
                <MDBInput label="Task Content" aria-label={"Task Content"} name="content" inputRef={register} required minlength="3" maxlength="25" />
                <MDBBtn color={"#5d5dff"} name="button" className={"light-color text-color"} innerRef={register} onClick={props.toggle} type="submit">Send</MDBBtn>
                </div>
            </form>
            </MDBCol>
        </MDBRow>
    </MDBModalBody>
    <MDBModalFooter className={"main-light-color"}>
        <MDBBtn color="red" onClick={CreateTaskStateToggle}>Close</MDBBtn>
    </MDBModalFooter>
    </MDBModal>
    </MDBContainer>
    );
}


import React from 'react';
import { MDBContainer,MDBModal, MDBModalBody, MDBModalHeader, MDBBtn, MDBCol, MDBRow,MDBInput } from 'mdbreact';
import  {useForm}  from "react-hook-form";
import axios from 'axios';

export default function EditTaskModal(props){
  function deleteTask(e){
      axios({
        method: 'DELETE',
        url: process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/task/'+props.item.id+'/delete': process.env.REACT_APP_PRO_MODE + '/task/'+props.item.id+'/delete',
        headers:{
          'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
        }
    }).catch(error => {
        console.log(error)
    }).then(response => {
        // props.refetch();
        props.client.publish({destination: '/app/socket/'+props.id, body: props.id.toString()});

    });
  }

  const { register, handleSubmit } = useForm();

  const onSubmit = (data, e) => {
        e.target.reset();
        const task = {
          icon: props.item.icon,
          status: props.item.status,
          title: data.title,
          content: data.content,
      };

      axios({
          method: 'PUT',
          url: process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/task/'+props.item.id+'/update': process.env.REACT_APP_PRO_MODE + '/task/'+props.item.id+'/update',
          headers: {
              'Content-Type': 'application/json',
              'Authorization': "Bearer " + localStorage.getItem("jwttoken"),

          },
          data: {
              icon: task.icon,
              status: task.status,
              title: task.title,
              content: task.content,
          }
      }).catch(error => {
          console.log(error)
      }).then(response => {
          // this is now called!
          props.client.publish({destination: '/app/socket/'+props.id, body: props.id.toString()});
        });
    };
  return (
    <MDBContainer>
      <MDBModal isOpen={props.state} toggle={props.toggle}>
        <MDBModalHeader className={"main-light-color"} toggle={props.toggle}>{props.item.title}</MDBModalHeader>
        <MDBModalBody className={"main-light-color"}>
        <MDBContainer>
        <MDBRow>
            <MDBCol md="6">
            <form  onSubmit={handleSubmit(onSubmit)}>
                <div className="grey-text">
                <MDBInput hidden valueDefault={props.item.id} inputRef={register} name="id" />
                <MDBInput data-testid={"edit-task-title"} valueDefault={props.item.title} inputRef={register} name="title" required minlength="3" maxlength="25" />
                <MDBInput data-testid={"edit-task-content"} valueDefault={props.item.content}  type="textarea" inputRef={register} outline name="content" required minlength="3" maxlength="25" />
                <MDBBtn color="#5d5dff" className={"light-color text-color"} type="submit" onClick={props.toggle}>Send</MDBBtn>
                <MDBBtn data-testid={"delete-task"} color={"black"} className={"light-color"} onClick={() => {props.toggle(); deleteTask() }}><i style={{color: "red", cursor: "pointer"}} onClick={() => deleteTask()} className="fas fa-trash-alt"></i></MDBBtn>

                </div>
            </form>
            </MDBCol>
        </MDBRow>
        </MDBContainer>
        </MDBModalBody>
      </MDBModal>
    </MDBContainer>
    );
}

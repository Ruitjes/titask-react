import {MDBContainer, MDBRow} from 'mdbreact';
import React , { useState, useEffect } from 'react';
import {MDBBox} from "mdbreact";
import axios from 'axios';
import CreateBoardModal from './CreateBoardModal';
import Item from './Item';


export default function Boards(props) {
    const [items, setItems] = useState([]);
    const [boardState, setBoardState] = useState(false);

    function refetchBoard() {
        axios
        .get(process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE +"/user/" + localStorage.getItem("userid") +"/boards" : process.env.REACT_APP_PRO_MODE +"/user/" + localStorage.getItem("userid") +"/boards",{
            headers: {
                'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
            }
        }).then((result) => {
            setItems(result.data)
        }).catch(error => {
            alert("Geen toegang, ben je ingelogd?")
            window.location = '/';
        });
    }

    useEffect(() => {
        //Is this my board
        axios
        .get(process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE +"/user/" + localStorage.getItem("userid") +"/boards" : process.env.REACT_APP_PRO_MODE +"/user/" + localStorage.getItem("userid") +"/boards",{
            headers: {
                'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
            }
        }).then((result) => {
            refetchBoard();
        }).catch(error => {
            alert("Geen toegang, ben je ingelogd?")
            window.location = '/';
            console.log(error)
        });
    }, []);

      function boardToggle() {
        setBoardState(!boardState);
    }

    return ( 
    <> 
    <MDBContainer style={{minHeight: "800px"}}>
    <CreateBoardModal
            // boardId={boardId}
            refetchBoard={refetchBoard}
            state={boardState}
            toggle={boardToggle}
    />
        <MDBBox display="flex" justifyContent="center">
            <MDBRow style={{width: "100%"}}>
                {items.map((item) =>
                <Item refetchBoard={refetchBoard} item={item} />
                )}
             </MDBRow>
        </MDBBox>
    </MDBContainer> 
    </>
    );
}
import React from "react";
import CreateBoardModal from "../CreateBoardModal";
import 'mutationobserver-shim';
global.MutationObserver = window.MutationObserver;
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { render, fireEvent, waitFor, screen } from '@testing-library/react'
import user from '@testing-library/user-event'

test('Render the CreateBoardModal', () => {
    render(<CreateBoardModal/>)

    //unity
    expect(screen.getByText("Create New Board"));

    fireEvent.click(screen.getByText("Create New Board"));

    expect(screen.getByText("Create Board"));

    expect(screen.getByLabelText("Name"));
    expect(screen.getByText("Description"));

    expect(screen.getByText("Create"));


  });
import React from "react";
import Item from "../Item";
import 'mutationobserver-shim';
global.MutationObserver = window.MutationObserver;
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { render, fireEvent, waitFor, screen } from '@testing-library/react'
import user from '@testing-library/user-event'

const item = {
        "id":14,
        "title":"blablabla",
        "description":"Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
        "cols":[
           {
              "id":10,
              "status":"TO DO",
              "icon":"x",
              "color":"#000000"
           },
           {
              "id":11,
              "status":"DONE",
              "icon":"x",
              "color":"#000000"
           },
           {
              "id":12,
              "status":"DOING",
              "icon":"x",
              "color":"#000000"
           }
        ],
        "tasks":[
           {
              "id":9,
              "icon":"x",
              "status":"TO DO",
              "taskOrder":0,
              "title":"TEST",
              "content":"TEST"
           },
           {
              "id":10,
              "icon":"x",
              "status":"TO DO",
              "taskOrder":0,
              "title":"TEST 1",
              "content":"TEST 1"
           },
           {
              "id":11,
              "icon":"x",
              "status":"DONE",
              "taskOrder":0,
              "title":"TEST 2",
              "content":"TEST 2"
           }
        ]
     }


test('Render the items', () => {
    render(<Router><Item item={item} /></Router>)
    const title = item.title;
    const description = item.description;

    //unity
    expect(screen.getByText(title));
    expect(screen.getByText(description));

  });
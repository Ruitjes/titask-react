import React from "react";
import Boards from "../Boards";
import 'mutationobserver-shim';
global.MutationObserver = window.MutationObserver;

import { render, fireEvent, waitFor, screen } from '@testing-library/react'
import user from '@testing-library/user-event'

test('Render the items', () => {
    render(<Boards />)
    
    expect(screen.getByText("Create New Board"))
    
    fireEvent.click(screen.getByText("Create New Board"))
    expect(screen.getByText("Create Board"))
    expect(screen.getByLabelText("Name"));
    expect(screen.getByLabelText("Description"));


    expect(screen.getByText("Create"))

    //unity
  
  });
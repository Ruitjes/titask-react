import React , { useState, useEffect, useCallback } from 'react';
import {MDBCol, MDBCard, MDBIcon, MDBCardTitle, MDBBtn, MDBLink} from "mdbreact";
import axios from 'axios';

export default function Item(props) {
    function deleteBoard(boardId){
        if(window.confirm("Weet je zeker dat je bord " + props.item.title + " wilt verwijderen"))
        {
            axios({
                method: 'DELETE',
                url: process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/board/' + boardId: process.env.REACT_APP_PRO_MODE + '/board/' + boardId,
                headers: {
                    'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
                }
            }).then(response => {
                props.refetchBoard();
                // alert("Je bord met id " + boardId + " is verwijderd")
            }).catch(error => {
                alert(error);
            });
        }
    }

return (
<MDBCol sm="4" key={props.item.id} style={{padding: "10px"}} >    
<MDBCard
    className='card-image'
    style={{
        backgroundImage:
        "url('https://mdbootstrap.com/img/Photos/Horizontal/Work/4-col/img%20%2814%29.jpg')"
    }}
    >
    <div className='text-white text-center d-flex rgba-black-strong align-items-center py-5 px-4'>
        <div>
        <h5 style={{color: "#5d5dff"}}>
            <MDBIcon style={{float: "left"}} icon='columns' /> Admin
            <MDBIcon data-testid={props.item.title.toLowerCase().split(' ').join('-')+ "-delete"} onClick={() => deleteBoard(props.item.id)} style={{float: "right", color: "red", cursor: "pointer"}} icon='trash-alt' />
        </h5>
        <MDBCardTitle tag='h3' className='pt-2'>
            <strong>{props.item.title}</strong>
        </MDBCardTitle>
        <p>
            {props.item.description}
        </p>
        <MDBLink to={"/board/" + props.item.id}>
            <MDBBtn className="light-color text-color" color='#5d5dff'>
                <MDBIcon icon='clone left' /> View Board
            </MDBBtn>        
        </MDBLink>                        
        </div>
    </div>
</MDBCard>
</MDBCol>

)}
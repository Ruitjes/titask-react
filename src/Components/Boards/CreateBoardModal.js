import React, { useRef, useState } from "react";
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBBox, MDBRow, MDBCol, MDBInput } from 'mdbreact';
import  {useForm}  from "react-hook-form";
import axios from 'axios';


export default function CreateBoardModal(props){
    const { register, handleSubmit } = useForm();
    const ref = useRef(null);
    
    const [createBoardModal, setCreateBoard] = useState();

    function CreateBoardModalToggle() {
        setCreateBoard(!createBoardModal);
    }

    const onSubmit = (data, e) => {
        e.target.reset();
            axios({
                method: 'post',
                url: process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/user/'+localStorage.getItem("userid")+'/board': process.env.REACT_APP_PRO_MODE + '/user/'+localStorage.getItem("userid")+'/board',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': "Bearer " + localStorage.getItem("jwttoken"),
                },
                data: {
                    title: data.name,
                    description: data.description,
                    tasks: [],
                    cols: [],
                }
            }).then(response => {
                alert("Nieuwe board " +data.name+ " aangemaakt")
                props.refetchBoard();
            }).catch(error => {
                alert(error)
                console.log(error)
            });
      };

  return (
    <MDBContainer style={{padding: "0px"}}>
    <MDBBox display="flex" justifyContent="end">
        <MDBBtn onClick={CreateBoardModalToggle} color={"#5d5dff"} className={"light-color"}>Create New Board</MDBBtn>
    </MDBBox>
    <MDBModal isOpen={createBoardModal} toggle={CreateBoardModalToggle}>
    <MDBModalHeader className={"main-light-color"} toggle={CreateBoardModalToggle} data-testid="create-new-board-header">Create Board</MDBModalHeader>
    <MDBModalBody className={"main-light-color"}>
        <MDBRow >
            <MDBCol md="6">
            <form onSubmit={handleSubmit(onSubmit)}>
                <MDBInput inputRef={register} aria-label={"Name"} id={"name"} label="Name" name="name" group error="wrong"
                        success="right" required minlength="3" maxlength="25" />

                <MDBInput inputRef={register} aria-label={"Description"} id={"description"} label="Description" name="description" group error="wrong"
                        success="right" required minlength="3" maxlength="50" />
                <MDBBtn color={"#5d5dff"} name="button" className={"light-color text-color"} innerRef={register} onClick={CreateBoardModalToggle} type="submit">Create</MDBBtn>
            </form>
            </MDBCol>
        </MDBRow>
    </MDBModalBody>
    <MDBModalFooter className={"main-light-color"}>
        <MDBBtn color="red" onClick={CreateBoardModalToggle}>Close</MDBBtn>
    </MDBModalFooter>
    </MDBModal>
    </MDBContainer>
    );
}


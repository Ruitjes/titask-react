import React, {useEffect} from 'react';
import {
    MDBCard,
    MDBCardBody,
    MDBCardImage,
    MDBCardTitle,
    MDBCardText,
    MDBRow,
    MDBCol,
    MDBBox,
    MDBBtn,
    MDBLink
} from 'mdbreact';
import Banner from '../../img/banner.jpg';
import { NavLink } from 'react-router-dom';

export default function LandingPage(){
    return (
        <MDBRow
            style={{
            width: "100%",
            margin: "0px",
            padding: "0px 0px 0px 0px"
        }}>
            <MDBCol style={{
                padding: "0px",
                maxWidth: "100%"
            }}>
                <MDBCard reverse>
                    <MDBCardImage
                        cascade
                        style={{
                        width: "100%",
                        height: '25rem',
                        boxShadow: 'inset 0 0 0 1000px rgba(0,0,0,.2)'
                    }}
                        src={Banner}/>
                    <MDBCardBody cascade className="text-center main-light-color">
                        <MDBCardTitle className={"text-color"}>TiTask</MDBCardTitle>
                        <h5
                            style={{
                            color: "#5d5dff"
                        }}>
                            <strong>TiTask</strong>
                        </h5>
                        <MDBBox className={"d-flex justify-content-center"}>
                            <MDBCardText
                                style={{
                                width: "500px"
                            }}>Lorem
                                ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Volutpat commodo sed egestas
                                egestas fringilla. Eu facilisis sed odio morbi quis. Felis imperdiet proin
                                fermentum leo. Lacus sed turpis tincidunt id aliquet risus feugiat in ante.
                                Viverra maecenas accumsan lacus vel facilisis volutpat est. Enim ut tellus
                                elementum sagittis vitae et leo duis ut. Odio tempor orci dapibus ultrices. Urna
                                et pharetra pharetra massa massa ultricies mi quis. Aenean pharetra magna ac
                                placerat vestibulum lectus mauris. Enim sed faucibus turpis in eu mi bibendum.
                                Lacus laoreet non curabitur gravida. Morbi tristique senectus et netus et
                                malesuada. Sed odio morbi quis commodo odio aenean.</MDBCardText>
                        </MDBBox>
                        <br/>
                        <a href="/boards"><MDBBtn className={"light-color"} >Go to the boards</MDBBtn></a>
                    </MDBCardBody>
                </MDBCard>
            </MDBCol>
        </MDBRow>
    )
}

import React from "react";
import LandingPage from "../LandingPage"

import 'mutationobserver-shim';
global.MutationObserver = window.MutationObserver;

import { render, fireEvent, waitFor, screen } from '@testing-library/react'

test("lorem ipsum", () => {
    render(<LandingPage />);

    expect(screen.getByText("Go to the boards"));
    expect(screen.findAllByText("TiTask"));
    expect(screen.getByText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Volutpat commodo sed egestas egestas fringilla. Eu facilisis sed odio morbi quis. Felis imperdiet proin fermentum leo. Lacus sed turpis tincidunt id aliquet risus feugiat in ante. Viverra maecenas accumsan lacus vel facilisis volutpat est. Enim ut tellus elementum sagittis vitae et leo duis ut. Odio tempor orci dapibus ultrices. Urna et pharetra pharetra massa massa ultricies mi quis. Aenean pharetra magna ac placerat vestibulum lectus mauris. Enim sed faucibus turpis in eu mi bibendum. Lacus laoreet non curabitur gravida. Morbi tristique senectus et netus et malesuada. Sed odio morbi quis commodo odio aenean."))

  });
// test/LoginForm.test.js
import '@testing-library/jest-dom'
import React, {useState} from 'react'
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import ModalLogin from '../ModalLogin'

import 'mutationobserver-shim';
global.MutationObserver = window.MutationObserver;

const username = "johndoe";
const password = "secret";
const url = process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/user/signin?username='+ username +'&password=' + password 
: process.env.REACT_APP_PRO_MODE + '/user/signin?username='+ username +'&password=' + password;

const fakeResponse = {jwttoken: "fake_user_jwttoken"}
const server = setupServer(
  rest.post(url, (req, res, ctx) => {
    // Respond with a mocked user jwttoken that gets persisted
    // in the `sessionStorage` by the `Login` component.
    return res(ctx.json('fake_user_jwttoken'))
  }),
)

// Enable API mocking before tests.
beforeAll(() => server.listen())

// Reset any runtime request handlers we may add during the tests.
afterEach(() => server.resetHandlers())

// Disable API mocking after the tests are done.
afterAll(() => server.close())

test('allows the user to log in', async () => {
  render(<ModalLogin />)
  userEvent.click(screen.getByText("Login"));

  userEvent.type(screen.getByLabelText("Your name"), username)
  userEvent.type(screen.getByLabelText("Your password"), password)
  
  userEvent.click(screen.getByText("Login Gebruiker"));

  await screen.findByText('Je bent ingelogd');
 
  expect(window.localStorage.getItem('jwttoken')).toEqual(fakeResponse.jwttoken)
})

test('handles login exception', async() => {
  server.use(
    rest.post(url, (req, res, ctx) => {
      // Respond with "500 Internal Server Error" status for this test.
      return res(
        ctx.status(500),
        ctx.json('Je gebruikersnaam en wachtwoord komen niet overeen op een account'),
      )
    }),
  )

  render(<ModalLogin />)

  userEvent.click(screen.getByText("Login"));

  userEvent.type(screen.getByLabelText("Your name"), username)
  userEvent.type(screen.getByLabelText("Your password"), password)
  
  userEvent.click(screen.getByText("Login Gebruiker"));

  await screen.findByText('Je gebruikersnaam en wachtwoord komen niet overeen op een account');

  expect(window.sessionStorage.getItem('jwttoken')).toBeNull()
})
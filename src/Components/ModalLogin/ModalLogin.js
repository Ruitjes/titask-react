import React, { useRef, useState } from "react";
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBLink, MDBCol, MDBInput, MDBAlert } from 'mdbreact';
import  {useForm}  from "react-hook-form";
import axios from 'axios';

export default function ModalLogin(props){
  const { register, handleSubmit } = useForm();
  const ref = useRef(null);
  const [error, setError] = useState("");
  const [loginModalState, setLoginModalState] = useState();
  // const [url, setUrl] = useState();
  // 

  function loginModalToggle() {
    setLoginModalState(!loginModalState);
  }

  async function onSubmit(data, e){
      e.target.reset();
      // const url = 'http://localhost:808k0/user/signin?username='+ data.username +'&password=' + data.password;
      await axios({
              method: 'post',
              url: process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/user/signin?username='+ data.username +'&password=' + data.password 
                                                          : process.env.REACT_APP_PRO_MODE + '/user/signin?username='+ data.username +'&password=' + data.password,
              headers: {
                  'Content-Type': 'application/json',
              },
          }).then(response => {
            console.log(response.data);
            window.localStorage.setItem('jwttoken', response.data);
            setError("Je bent ingelogd");
            setTimeout(() => {
              props.checkIfLoggedIn();
              loginModalToggle(); 
            }, 1000);
          }).catch(error => {
            setError("Je gebruikersnaam en wachtwoord komen niet overeen op een account");
        });
    }

  return (
    <MDBContainer>          
      <a onClick={loginModalToggle} className="nav-link Ripple-parent">
        Login
      </a>
      <MDBModal isOpen={loginModalState} toggle={loginModalToggle}>
        <MDBModalHeader className={"main-light-color"} toggle={loginModalToggle}>User</MDBModalHeader>
          <MDBModalBody className={"main-light-color"}>
            <MDBCol size="12">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <p className="h5 text-center mb-6 mt-3">Sign In</p>
                      {error ?  (
                        <>
                          <MDBAlert role="alert" color="primary" >
                            {error}
                          </MDBAlert>
                        </>
                      ) : (
                        <>

                        </>
                      )}
                        <MDBInput style={{marginLeft: "45px"}} inputRef={register} aria-label={"Your name"} id={"username"} label="Your name" name="username" icon="user" group type="text" validate error="wrong"
                        success="right" required minlength="4" maxlength="255" />
                        <MDBInput style={{marginLeft: "45px"}} inputRef={register} aria-label={"Your password"} id={"password"} label="Your password" icon="lock" name="password" group type="password" validate required minlength="5" maxlength="255"/>

                        <MDBBtn color="red" onClick={loginModalToggle}>Close</MDBBtn>
                        <MDBBtn color="#5d5dff" name="button" type={"submit"} innerRef={register} className={"light-color"}>Login Gebruiker</MDBBtn>
                </form>
            </MDBCol>
          </MDBModalBody>
      </MDBModal>
    </MDBContainer>
    );
  }
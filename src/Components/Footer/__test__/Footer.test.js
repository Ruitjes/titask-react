import React from "react";
import Footer from "../Footer"

import 'mutationobserver-shim';
global.MutationObserver = window.MutationObserver;

import { render, fireEvent, waitFor, screen } from '@testing-library/react'

test("lorem ipsum", () => {
    render(<Footer />);

    expect(screen.getByText("Titask is place to organize your todos"));
    expect(screen.getByText("Links"));

    expect(screen.getByText("Links"));
    expect(screen.getByText("Link 1"));
    expect(screen.getByText("Link 2"));
    expect(screen.getByText("Link 3"));
    expect(screen.getByText("Link 4"));
  });
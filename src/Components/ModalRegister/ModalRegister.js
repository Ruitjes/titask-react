import React, { useRef, useState } from "react";
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBAlert, MDBLink, MDBCol, MDBInput } from 'mdbreact';
import  {useForm}  from "react-hook-form";
import axios from 'axios';


export default function ModalRegister(props){
  const { register, handleSubmit } = useForm();
  const ref = useRef(null);
  const [error, setError] = useState("");
  const [registerModalState, setRegisterModalState] = useState();

  function registerModalToggle() {
    setRegisterModalState(!registerModalState);
  }

  const onSubmit = (data, e) => {
      e.target.reset();
          axios({
              method: 'post',
              url: process.env.NODE_ENV === 'development' ? process.env.REACT_APP_DEV_MODE + '/user/signup': process.env.REACT_APP_PRO_MODE + '/user/signup',
              headers: {
                  'Content-Type': 'application/json',
              },
              data: {
                  username: data.username,
                  email: data.email,
                  password: data.password,
                  roles: ["ROLE_CLIENT"]
              }
          }).then(response => {
            localStorage.setItem('jwttoken', response.data);
            setError("Account aangemaakt, je wordt automatisch ingelogd...");
            setTimeout(() => {
              props.checkIfLoggedIn();
              registerModalToggle(); 
            }, 1000);
          }).catch(error => {
            console.log(error)
            setError("Er is iets fout gegaan met regristeren.")
        });
    };

  return (
    <MDBContainer>          
      <a onClick={registerModalToggle} className="nav-link Ripple-parent">
        Register
      </a>
      <MDBModal isOpen={registerModalState} toggle={registerModalToggle}>
        <MDBModalHeader className={"main-light-color"} toggle={registerModalToggle}>Sign up</MDBModalHeader>
          <MDBModalBody className={"main-light-color"}>
            <MDBCol size="12">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <p className="h5 text-center mb-6 mt-3">Sign up</p>
                    {error ?  (
                      <>
                        <MDBAlert color="danger" >
                          {error}
                        </MDBAlert>
                      </>
                    ) : (
                      <>

                      </>
                    )}
                    <div className="">
                        <MDBInput style={{marginLeft: "45px"}} inputRef={register} label="Your name" aria-label="Your name" name="username" icon="user" group type="text" validate error="wrong"
                        success="right" 
                        required minlength="4" maxlength="25" />
                        <MDBInput style={{marginLeft: "45px"}} inputRef={register} label="Your email" aria-label="Your email" icon="envelope" group name="email" type="email" validate error="wrong"
                        success="right" 
                        required minlength="4" maxlength="25" />
                        <MDBInput style={{marginLeft: "45px"}} inputRef={register} label="Your password" aria-label="Your password" icon="lock" name="password" group type="password" 
                        required minlength="4" maxlength="25" />

                        <MDBBtn color="red" onClick={props.toggle}>Close</MDBBtn>
                        <MDBBtn data-testid="register-button" color="#5d5dff" name="button" type={"submit"} innerRef={register} className={"light-color"}>Register</MDBBtn>
                    </div>
                </form>
            </MDBCol>
          </MDBModalBody>
      </MDBModal>
    </MDBContainer>
    );
  }
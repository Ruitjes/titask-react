import '@testing-library/cypress/add-commands';
import '@4tw/cypress-drag-drop'


describe('Test the whole TiTask App', () => {
    beforeEach(() => {
      // Clear database for example
    })

    it('Test App', () => {
      // Visit App
      cy.visit('http://localhost:3000')

      // Register  
      cy.findByText("Login").click();

      cy.findByLabelText(/Your name/i).wait(250).type("admin")
      cy.findByLabelText(/Your password/i).wait(250).type("admin")
  
      cy.findByText("Login Gebruiker").click()
  
      cy.findByText("Je bent ingelogd").wait(2500)      

      // Create Board
      cy.findByText("Go to the boards").click()
      cy.findByText("Create New Board").click()

      cy.findByLabelText(/Name/i).wait(250).type("Cypress Test Board")
      cy.findByLabelText(/Description/i).wait(250).type("Lorem Ipsum")

      cy.findByText("Create").click()

      // Go to the boards
      cy.findByText("View Board").click()

      // Add a Column to the board
      cy.findByText("Add Column").click();
      cy.findByLabelText(/Column Name/i).wait(250).type("Doing")
      cy.findByText("Send").click().wait(1000)
      cy.findByText("DOING").should("be.visible");

      cy.findByText("Add Column").click();
      cy.findByLabelText(/Column Name/i).wait(250).type("Done")
      cy.findByText("Send").click().wait(1000)
      cy.findByText("DONE").should("be.visible");

      // Add a Task to the board
      cy.findByText("Add Task").click();
      cy.findByLabelText(/Task Name/i).wait(250).type("React Tests maken")
      cy.findByLabelText(/Task Content/i).wait(250).type("React Tests maken")
      cy.findByText("Send").click();
      cy.findByText("React Tests maken").should("be.visible");
      
      // Drag task
      cy.findByTestId("react-tests-maken-task-move").drag(".done-drop")
      cy.wait(1000);

      // Edit a Task
      cy.findByText("React Tests maken").click();

      cy.findByTestId("edit-task-title").wait(250).clear().type("Edit my Test Task")
      cy.findByTestId("edit-task-content").wait(250).clear().type("Edit my Test Task")
      cy.findByText("Send").click();

      cy.findByText("Edit my Test Task").should("be.visible");
      cy.wait(1000)

      // Delete a Task
      cy.findByText("Edit my Test Task").click();
      cy.wait(1000)
      cy.findByTestId("delete-task").click();
      cy.findByText("Edit my Test Task").should("not.exist");

      // Delete the Columns
      cy.findByTestId("done-delete").wait(250).click()
      cy.findByText("DONE").should("not.exist");

      cy.findByTestId("doing-delete").wait(250).click()
      cy.findByText("DOING").should("not.exist");

      // Go back to the board
      cy.findByText("< Back to the boards").click()
      cy.findByText("Cypress Test Board").should("be.visible")
      cy.findByText("Lorem Ipsum").should("be.visible")

      // Remove the board
      cy.findByTestId("cypress-test-board-delete").click();
      cy.findByText("Cypress Test Board").should("not.exist")
      cy.findByText("Lorem Ipsum").should("not.exist")

      // Logout
      cy.findByText("Logout").click();
      cy.findByText("Login").should("be.visible")

    })
  })